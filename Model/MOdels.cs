﻿using FluentValidation;

namespace Model
{
    public class User
    {
        public string Name { get; set; }
    }

    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Length(1, 1);
        }
    }
}