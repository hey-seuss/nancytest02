﻿using System.Runtime.InteropServices;
using FluentValidation;
using Model;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Validation;

namespace Web
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Post["/"] = _ =>
            {
                User user = this.Bind();
                var result = this.Validate(user);

                return result.IsValid
                    ? HttpStatusCode.OK
                    : HttpStatusCode.InternalServerError;
            };

            Post["/potato"] = _ =>
            {
                Potato potato = this.Bind();
                var result = this.Validate(potato);

                return result.IsValid
                    ? HttpStatusCode.OK
                    : HttpStatusCode.InternalServerError;
            };
        }
    }

    public class Potato
    {
        public string Name { get; set; }
    }
    
    // Uncomment this and both validators work; leave it commented,
    // and neither work (but UserValidator should since it exists
    // in the other assembly).

    //public class PotatoValidator : AbstractValidator<Potato>
    //{
    //    public PotatoValidator()
    //    {
    //        RuleFor(x => x.Name)
    //            .NotEmpty()
    //            .Length(1, 1);
    //    }
    //}
}